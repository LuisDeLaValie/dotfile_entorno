# Eliminar aplicacion no deseadas
sudo dnf remove -y firefox libreoffice\*

sudo dnf update


# Instalar snap
sudo dnf install snapd
sudo ln -s /var/lib/snapd/snap /snap

# Instalar aplicacion nesesarias
sudo snap install spotify postman code nordpass \\
    whatsapp-linux-app libreoffice

# Actualizar aplicacions
sudo snap refresh

# Instatlar flatpak
sudo dnf install flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# Instalar dependencias
flatpak install flathub eu.betterbird.Betterbird

# actualizar dependencias
flatpak update
