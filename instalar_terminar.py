import os
import subprocess
import sys

def install_kitty():
    try:
        subprocess.run(["curl", "-L", "https://sw.kovidgoyal.net/kitty/installer.sh"], check=True)
        subprocess.run(["sh", "/dev/stdin"], stdin=subprocess.PIPE, cwd=os.path.expanduser("~/.local/kitty.app/bin/"))
        os.symlink("~/.local/kitty.app/bin/kitty", "~/.local/kitty.app/bin/kitten", "~/.local/bin/")
        subprocess.run(["cp", "~/.local/kitty.app/share/applications/kitty.desktop", "~/.local/share/applications/"], check=True)
        subprocess.run(["cp", "~/.local/kitty.app/share/applications/kitty-open.desktop", "~/.local/share/applications/"], check=True)
        subprocess.run(["sed", "-i", f"s|Icon=kitty|Icon={os.path.expanduser('~')}/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g", "~/.local/share/applications/kitty*.desktop"], check=True)
        subprocess.run(["sed", "-i", f"s|Exec=kitty|Exec={os.path.expanduser('~')}/.local/kitty.app/bin/kitty|g", "~/.local/share/applications/kitty*.desktop"], check=True)
            
    except Exception as identifier:
        print(f"Error al instalar kitty:\n\t {identifier}")
        sys.exit(1) 


def add_dracula_theme_to_kitty():
    try:
        subprocess.run(["curl", "-L", "-o", "kitty.zip", "https://github.com/dracula/kitty/archive/master.zip"], check=True)
        subprocess.run(["unzip", "kitty.zip"], check=True)
        subprocess.run(["cp", "dracula.conf", "diff.conf", "~/.config/kitty/"], check=True)
        with open(os.path.expanduser("~/.config/kitty/kitty.conf"), "a") as f:
            f.write("include dracula.conf\n")
        subprocess.run(["rm", "-rf", "kitty-master", "kitty.zip"], check=True)
        
    except Exception as identifier:
        print(f"Error al instalar kitty:\n\t {identifier}")
        sys.exit(1) 


def configure_kitty():


    try:
        subprocess.run(["sh", "-c", "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"], check=True)
        subprocess.run(["curl", "-s", "https://ohmyposh.dev/install.sh", "|", "bash", "-s"], check=True)
        subprocess.run(["oh-my-posh", "font", "install"], check=True)
        with open(os.path.expanduser("~/.zshrc"), "a") as f:
            f.write("eval \"$(oh-my-posh init zsh)\"\n")
        subprocess.run(["zsh"], check=True)
        
    except Exception as identifier:
        print(f"Error al instalar kitty:\n\t {identifier}")
        sys.exit(1) 



def start():
    # Instalar Kitty
    install_kitty()

    # Agregar tema Drácula a Kitty
    add_dracula_theme_to_kitty()

    # Configurar Kitty
    configure_kitty()


if __name__ == "__main__":
    start()
