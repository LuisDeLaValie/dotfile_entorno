

# Para mas informacion ver https://sw.kovidgoyal.net/kitty/binary/#manually-installing
install_kitty() {
    echo "Installing kitty..."
    curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
    ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
    cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
    cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
    sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
    sed -i "s|Exec=kitty|Exec=/home/$USER/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
}

# para mas informacion ver https://draculatheme.com/kitty
add_dracula_theme_to_kitty() {
    echo "Adding dracula theme to kitty..."
    curl -L -o kitty.zip  https://github.com/dracula/kitty/archive/master.zip
    unzip kitty.zip
    cd kitty-master
    cp dracula.conf diff.conf ~/.config/kitty/
    echo "include dracula.conf" >> ~/.config/kitty/kitty.conf
    cd .. && rm -rf kitty-master kitty.zip
    
}


configurar_terminal() {
    echo "Configurando kitty..."
    # Instalar zsh
    sudo dnf install -y zsh
    chsh -s $(which zsh)

    # Instalr oh my zsh    
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    # Instalar oh my posh
    curl -s https://ohmyposh.dev/install.sh | bash -s
    echo 'eval "$(oh-my-posh init zsh --config https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/dracula.omp.json)"' >> ~/.zshrc
    exec zsh

    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

}

configurar_terminal


# Instalar Kitty
install_kitty

# Agregar tema Drácula a Kitty
add_dracula_theme_to_kitty

