#!/bin/bash

# Verifica si /etc/os-release existe y es legible
if [ -r /etc/os-release ]; then
    # Lee la información de distribución desde /etc/os-release
    source /etc/os-release
    # Imprime el nombre de la distribución
    echo "Estás utilizando $NAME"
else
    # Si /etc/os-release no está presente o no es legible, imprime un mensaje de error
    echo "No se pudo determinar la distribución de Linux"
fi
